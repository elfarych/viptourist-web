
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },

  {
    path: '/privacy-policy',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'policy', component: () => import('src/pages/docs/page-privacy-policy') }
    ]
  },

  {
    path: '/user-agreement',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'agreements', component: () => import('src/pages/docs/page-user-agreements') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
